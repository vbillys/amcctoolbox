function [ P, M ] = makecamera( K, R, C )
% [ P, M ] = MAKECAMERA( K, R, C ) Generate a camera from component
% matrices
%   Given an intrinsics matrix K = [fx gamma cx; 0 fy cy; 0 0 1],
%   rotation matrix R and camera centre C, returns the camera matrix P
%   and its component pose matrix M

% Copyright (c) Michael Warren 2014

% This file is part of the AMMCC Toolbox.

% The AMMCC Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% The AMMCC Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with the AMMCC Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    M = [R -R*C];
    P = K*M;

end

